package com.apptec.ejemplo.client;

import com.apptec.ejemplo.shared.Alumnos;
import com.apptec.ejemplo.shared.FieldVerifier;
import com.apptec.ejemplo.shared.*;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.event.shared.SimpleEventBus;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;
import com.google.web.bindery.requestfactory.shared.Receiver;

import java.util.Arrays;
import java.util.List;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class escolares implements EntryPoint {
  /**
   * The message displayed to the user when the server cannot be reached or
   * returns an error.
   */
  private static final String SERVER_ERROR = "An error occurred while "
      + "attempting to contact the server. Please check your network "
      + "connection and try again.";

  /**
   * Create a remote service proxy to talk to the server-side Greeting service.
   */
  private final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);

  /**
   * This is the entry point method.
   */
  private static class Materia {
    private final String classroom;
    private final String name;
    private final String time;
    private final String profesor;

    public Materia(String name, String classroom, String time, String profesor) {
      this.name = name;
      this.classroom = classroom;
      this.time = time;
      this.profesor = profesor;
    }
  }
  // The list of data to display.

   EscolaresRequestFactory requestFactory;
  final EventBus eventBus = new SimpleEventBus();


  public void onModuleLoad() {
    final Label pba = new Label();

    final Button sendButton = new Button("Registrate!");

    final TextBox nameField = new TextBox();
    nameField.setText("Nombre de alumno");

    final TextBox apellidoPField = new TextBox();
    apellidoPField.setText("Apellido paterno");

    final TextBox apellidoMField = new TextBox();
    apellidoMField.setText("Apellido materno");

    final TextBox telefonoField = new TextBox();
    telefonoField.setText("Teléfono");

    final TextBox tutorField = new TextBox();
    tutorField.setText("Tutor");

    final Label errorLabel = new Label();

    final Label nombreTabla = new Label();
    // We can add style names to widgets
    sendButton.addStyleName("sendButton");
    nombreTabla.setText("Tabla de alumnos registrados");
    // Add the nameField and sendButton to the RootPanel
    // Use RootPanel.get() to get the entire body element
    RootPanel.get("nameFieldContainer").add(nameField);
    RootPanel.get("nameFieldContainer").add(apellidoPField);
    RootPanel.get("nameFieldContainer").add(apellidoMField);
    RootPanel.get("nameFieldContainer").add(telefonoField);
    RootPanel.get("nameFieldContainer").add(tutorField);
    RootPanel.get("nameFieldContainer").add(nombreTabla);
    RootPanel.get("nameFieldContainer").add(sendButton);


    CellTable<Materia> tableMaterias = new CellTable<Materia>();
    // Create name column.
    TextColumn<Materia> nameColumn = new TextColumn<Materia>() {
      @Override
      public String getValue(Materia materia) {
        return materia.name;
      }
    };

    // Create address column.
    TextColumn<Materia> classroomColumn = new TextColumn<Materia>() {
      @Override
      public String getValue(Materia materia) {
        return materia.classroom;
      }
    };
    TextColumn<Materia> timeColumn = new TextColumn<Materia>() {
      @Override
      public String getValue(Materia materia) {
        return materia.time;
      }
    };
    TextColumn<Materia> profColumn = new TextColumn<Materia>() {
      @Override
      public String getValue(Materia materia) {
        return ( materia.profesor);
      }
    };

    // Add the columns.
    tableMaterias.addColumn(nameColumn, "Nombre");
    tableMaterias.addColumn(classroomColumn, "Apellidos");
    tableMaterias.addColumn(timeColumn, "Teléfono");
    tableMaterias.addColumn(profColumn, "Tutor");


    // Set the total row count. This isn't strictly necessary, but it affects
    // paging calculations, so its good habit to keep the row count up to date.
    //tableMaterias.setRowCount(MATERIAS.size(), true);

    // Push the data into the widget.
    //tableMaterias.setRowData(0, MATERIAS);

    RootPanel.get("nameFieldContainer").add(tableMaterias);



    // Focus the cursor on the name field when the app loads
    nameField.setFocus(true);
    nameField.selectAll();

    // Create the popup dialog box
    final DialogBox dialogBox = new DialogBox();
    dialogBox.setText("Remote Procedure Call");
    dialogBox.setAnimationEnabled(true);
    final Button closeButton = new Button("Close");

    // We can set the id of a widget by accessing its Element
    closeButton.getElement().setId("closeButton");
    final Label textToServerLabel = new Label();
    final HTML serverResponseLabel = new HTML();
    HorizontalPanel dialogVPanel = new HorizontalPanel();
    dialogVPanel.addStyleName("dialogVPanel");
    dialogVPanel.add(new HTML("<b>Sending name to the server:</b>"));
    dialogVPanel.add(textToServerLabel);
    dialogVPanel.add(new HTML("<br><b>Server replies:</b>"));
    dialogVPanel.add(serverResponseLabel);
    dialogVPanel.setHorizontalAlignment(VerticalPanel.ALIGN_RIGHT);
    dialogVPanel.add(closeButton);
    dialogBox.setWidget(dialogVPanel);

    // Add a handler to close the DialogBox
    closeButton.addClickHandler(new ClickHandler() {
      public void onClick(ClickEvent event) {
        dialogBox.hide();
        sendButton.setEnabled(true);
        sendButton.setFocus(true);
      }
    });

final Label equis = new Label();
    requestFactory = GWT.create(EscolaresRequestFactory.class);
    requestFactory.initialize(eventBus);
    requestFactory.alumnoContext().holi().fire(
            new Receiver<String>() {
              @Override
              public void onSuccess(String alumno) {
                equis.setText(alumno);
                Window.alert("" + alumno);
              }
            });

    RootPanel.get("nameFieldContainer").add(equis);
    // Create a handler for the sendButton and nameField
    /*
    class MyHandler implements ClickHandler, KeyUpHandler {

      public void onClick(ClickEvent event) {
        sendNameToServer();
      }

      public void onKeyUp(KeyUpEvent event) {
        if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
          sendNameToServer();
        }
      }


      private void sendNameToServer() {
        // First, we validate the input.
        errorLabel.setText("");
        String textToServer = nameField.getText();
        if (!FieldVerifier.isValidName(textToServer)) {
          errorLabel.setText("Please enter at least four characters");
          return;
        }
        
        // Then, we send the input to the server.
        sendButton.setEnabled(false);
        textToServerLabel.setText(textToServer);
        serverResponseLabel.setText("");
        greetingService.greetServer(textToServer, new AsyncCallback<String>() {
          public void onFailure(Throwable caught) {
            // Show the RPC error message to the user
            dialogBox.setText("Remote Procedure Call - Failure");
            serverResponseLabel.addStyleName("serverResponseLabelError");
            serverResponseLabel.setHTML(SERVER_ERROR);
            dialogBox.center();
            closeButton.setFocus(true);
          }

          public void onSuccess(String result) {
            dialogBox.setText("Remote Procedure Call");
            serverResponseLabel.removeStyleName("serverResponseLabelError");
            serverResponseLabel.setHTML(result);
            dialogBox.center();
            closeButton.setFocus(true);
          }
        });
      }
    }*/

    // Add a handler to send the name to the server
    //MyHandler handler = new MyHandler();
    //sendButton.addClickHandler(handler);
    //nameField.addKeyUpHandler(handler);
  }
}
