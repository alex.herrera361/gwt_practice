package com.apptec.ejemplo.client;

import com.apptec.ejemplo.server.MyServiceLocator;
import com.apptec.ejemplo.server.methods.prueba;
import com.apptec.ejemplo.shared.ProxyAlumnos;

import com.google.web.bindery.requestfactory.shared.Request;
import com.google.web.bindery.requestfactory.shared.RequestContext;
import com.google.web.bindery.requestfactory.shared.Service;

import java.util.List;

@Service(value =  prueba.class, locator = MyServiceLocator.class)
public interface AlumnoContext extends RequestContext {

Request <String> holi();
}
