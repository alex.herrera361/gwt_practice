package com.apptec.ejemplo.client;

import com.google.web.bindery.requestfactory.shared.RequestFactory;

public interface EscolaresRequestFactory extends RequestFactory {

    AlumnoContext alumnoContext();

}
