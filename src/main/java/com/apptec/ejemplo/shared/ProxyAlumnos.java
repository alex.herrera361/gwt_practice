package com.apptec.ejemplo.shared;

import com.google.web.bindery.requestfactory.shared.ProxyFor;

@ProxyFor(Alumnos.class)
public interface ProxyAlumnos {

    public ProxyTutores getTutor();

    public void setTutor(ProxyTutores tutor);

    public int getId_alumno() ;

    public void setId_alumno(int id_alumno);

    public String getNombre();

    public void setNombre(String nombre);

    public String getApellido_paterno();

    public void setApellido_paterno(String apellido_paterno);

    public String getApellido_materno();

    public void setApellido_materno(String apellido_materno);

    public String getTelefono();

    public void setTelefono(String telefono);

}
