package com.apptec.ejemplo.shared;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "alumnos")
public class Alumnos {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID_ALUMNO")
    private int id_alumno;
    @Column(name = "NOMBRE")
    private String nombre;
    @Column(name = "APELLIDO_PATERNO")
    private String apellido_paterno;
    @Column(name = "APELLIDO_MATERNO")
    private String apellido_materno;
    @Column(name = "TELEFONO")
    private String telefono;

    @Transient
    int version;


    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_TUTOR")
    Tutores tutor;

    /*
    @ManyToMany(fetch = FetchType.LAZY, targetEntity = Materias.class, cascade = CascadeType.ALL)
    @JoinTable(name = "alumnos_materias", joinColumns = @JoinColumn(name = "ID_ALUMNO"), inverseJoinColumns = @JoinColumn(name = "ID_MATERIA"))
    private List<Materias> materia;*/

    public Alumnos() {
    }

    /*public List<Materias> getMateria() {
        return materia;
    }

    public void setMateria(List<Materias> materia) {
        this.materia = materia;
    }*/

    public Alumnos(String id_alumno, String nombre, String apellido_paterno, String apellido_materno, String telefono, Tutores tutor) {
        this.nombre = nombre;
        this.apellido_paterno = apellido_paterno;
        this.apellido_materno = apellido_materno;
        this.telefono = telefono;
        this.tutor = tutor;
    }

    public Tutores getTutor() {
        return tutor;
    }

    public void setTutor(Tutores tutor) {
        this.tutor = tutor;
    }

    public int getId_alumno() {
        return id_alumno;
    }

    public void setId_alumno(int id_alumno) {
        this.id_alumno = id_alumno;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido_paterno() {
        return apellido_paterno;
    }

    public void setApellido_paterno(String apellido_paterno) {
        this.apellido_paterno = apellido_paterno;
    }

    public String getApellido_materno() {
        return apellido_materno;
    }

    public void setApellido_materno(String apellido_materno) {
        this.apellido_materno = apellido_materno;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
}
