package com.apptec.ejemplo.shared;


import javax.persistence.*;


import javax.persistence.*;

@Entity
@Table(name = "tutores")
public class Tutores {
    @Id
    @Column(name = "ID_TUTOR")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Column(name = "NOMBRE")
    private String nombre;
    @Column(name = "APELLIDO_PATERNO")
    private String ap_pat;
    @Column(name = "APELLIDO_MATERNO")
    private String ap_mat;
    @Column(name = "TELEFONO")
    private String telefono;
    @OneToOne(mappedBy = "Tutor")
    Alumnos alumno;
    @Transient
    int version;
    public Tutores(String nombre, String ap_pat, String ap_mat, String telefono, Alumnos alumno) {
        this.nombre = nombre;
        this.ap_pat = ap_pat;
        this.ap_mat = ap_mat;
        this.telefono = telefono;
        this.alumno = alumno;
    }

    public Tutores(){

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getAp_pat() {
        return ap_pat;
    }

    public void setAp_pat(String ap_pat) {
        this.ap_pat = ap_pat;
    }

    public String getAp_mat() {
        return ap_mat;
    }

    public void setAp_mat(String ap_mat) {
        this.ap_mat = ap_mat;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public Alumnos getAlumno() {
        return alumno;
    }

    public void setAlumno(Alumnos alumno) {
        this.alumno = alumno;
    }
}