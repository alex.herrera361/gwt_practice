package com.apptec.ejemplo.shared;

import java.util.List;

public interface ProxyProfesores {

    public List<ProxyMaterias> getProxyMaterias();

    public void setProxyMaterias(List<ProxyMaterias> materia);

    public int getId_prof();

    public void setId_prof(int id_prof);

    public String getNombre();

    public void setNombre(String nombre);

    public String getApellido_paterno();

    public void setApellido_paterno(String apellido_paterno);

    public String getApellido_materno();

    public void setApellido_materno(String apellido_materno);

    public String getRfc();

    public void setRfc(String rfc);
}
