package com.apptec.ejemplo.shared;


import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "profesores")
public class Profesores {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID_PROF")
    private int id_prof;
    @Column(name = "NOMBRE")
    private String nombre;
    @Column(name = "APELLIDO_PATERNO")
    private String apellido_paterno;
    @Column(name = "APELLIDO_MATERNO")
    private String apellido_materno;
    @Column(name = "RFC")
    private String rfc;

    @OneToMany(fetch=FetchType.LAZY, mappedBy="profesor", targetEntity=Materias.class, cascade=CascadeType.ALL)
    List<Materias> materia;

    @Transient
    int version;

    public Profesores(){}

    public Profesores(String nombre, String apellido_paterno, String apellido_materno, String rfc, List<Materias> materia) {
        this.materia = materia;
        this.id_prof = id_prof;
        this.nombre = nombre;
        this.apellido_paterno = apellido_paterno;
        this.apellido_materno = apellido_materno;
        this.rfc = rfc;
    }

    public List<Materias> getMateria() {
        return materia;
    }

    public void setMateria(List<Materias> materia) {
        this.materia = materia;
    }

    public int getId_prof() {
        return id_prof;
    }

    public void setId_prof(int id_prof) {
        this.id_prof = id_prof;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido_paterno() {
        return apellido_paterno;
    }

    public void setApellido_paterno(String apellido_paterno) {
        this.apellido_paterno = apellido_paterno;
    }

    public String getApellido_materno() {
        return apellido_materno;
    }

    public void setApellido_materno(String apellido_materno) {
        this.apellido_materno = apellido_materno;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }
}
