package com.apptec.ejemplo.shared;

public interface ProxyTutores {

    public int getId();

    public void setId(int id);

    public String getNombre();

    public void setNombre(String nombre);

    public String getAp_pat();

    public void setAp_pat(String ap_pat);

    public String getAp_mat();

    public void setAp_mat(String ap_mat);

    public String getTelefono();

    public void setTelefono(String telefono);

    public ProxyAlumnos getAlumno();

    public void setAlumno(ProxyAlumnos alumno);
}
