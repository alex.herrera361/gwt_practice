package com.apptec.ejemplo.shared;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "materias")
public class Materias {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID_MATERIA")
    private int id_materia;
    @Column(name = "NOMBRE")
    private String nombre;
    @Column(name = "HORARIO")
    private String horario;
    @Column(name = "SALON")
    private String salon;

    @ManyToOne(targetEntity = Profesores.class, optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_PROFESOR")

    Profesores profesor;

    @ManyToMany(fetch = FetchType.LAZY, cascade = { CascadeType.REFRESH, CascadeType.PERSIST }, targetEntity = Alumnos.class, mappedBy = "materia")
    private List<Alumnos> alumno;

    @Transient
    int version;


    public Materias(){}

    public Materias( String nombre, String horario, String salon, Profesores profesor) {
        this.profesor = profesor;
        this.nombre = nombre;
        this.horario = horario;
        this.salon = salon;
        this.profesor = profesor;
    }

    public int getId_materia() {
        return id_materia;
    }

    public void setId_materia(int id_materia) {
        this.id_materia = id_materia;
    }

    public Profesores getProfesor() {
        return profesor;
    }

    public List<Alumnos> getAlumno() {
        return alumno;
    }

    public void setAlumno(List<Alumnos> alumno) {
        this.alumno = alumno;
    }

    public void setProfesor(Profesores profesor) {
        this.profesor = profesor;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }

    public String getSalon() {
        return salon;
    }

    public void setSalon(String salon) {
        this.salon = salon;
    }
}