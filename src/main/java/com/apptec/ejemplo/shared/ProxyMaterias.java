package com.apptec.ejemplo.shared;

import java.util.List;

public interface ProxyMaterias {

    public int getId_materia();

    public void setId_materia(int id_materia);

    public ProxyProfesores getProfesor();

    public List<ProxyAlumnos> getAlumno();

    public void setAlumno(List<ProxyAlumnos> alumno);

    public void setProfesor(ProxyProfesores profesor);

    public String getNombre();

    public void setNombre(String nombre);

    public String getHorario();

    public void setHorario(String horario);

    public String getSalon();

    public void setSalon(String salon);
}
