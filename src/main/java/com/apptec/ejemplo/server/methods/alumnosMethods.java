package com.apptec.ejemplo.server.methods;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.SessionFactory;

import org.hibernate.cfg.Configuration;
import java.util.List;
import java.util.Iterator;
import com.apptec.ejemplo.shared.*;

public class alumnosMethods {

    static SessionFactory factory;

    public alumnosMethods() {
        try {
            factory = new Configuration().configure().buildSessionFactory();
        } catch (Throwable ex) {
            System.err.println("Failed to create sessionFactory object." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }



    //Register
    public  Integer addAlumno(String id_alumno,String nombre, String apellido_paterno, String apellido_materno, String telefono, Tutores tutor){
        Session session = factory.openSession();
        Transaction tx = null;
        Integer alumnoID = null;

        try {
            tx = session.beginTransaction();
            Alumnos alumno = new Alumnos(id_alumno,nombre, apellido_paterno, apellido_materno, telefono, tutor);
            alumnoID = (Integer) session.save(alumno);
            tx.commit();
        } catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return alumnoID;
    }

    // Method to  READ
      public  String listAlumnos(){
/*
        Session session = factory.openSession();
        Transaction tx = null;
        List<Alumnos> AlumnosLst = null;
        try {
            tx = session.beginTransaction();
            List<Alumnos> Al = session.createQuery("FROM Alumnos").list();
            AlumnosLst = Al;
            for (Iterator iterator = Al.iterator(); iterator.hasNext();){
                Alumnos alumno = (Alumnos) iterator.next();
                System.out.print("Nombre: " + alumno.getNombre());
                System.out.print("  Apellidos: " + alumno.getApellido_paterno() + " " + alumno.getApellido_materno());
                System.out.println("  Telefono: " + alumno.getTelefono());
            }
            tx.commit();
        } catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }*/
          String x = "Holi";
        return x;
    }

    // Method to UPDATE atrubute
    public  void updateAlumno(Integer alumnoID, String nombre ){
        Session session = factory.openSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();
            Alumnos alumno = (Alumnos)session.get(Alumnos.class, alumnoID);
            alumno.setNombre(nombre);
            session.update(alumno);
            tx.commit();
        } catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    // Method to DELETE an employee from the records
    public  void deleteEmployee(Integer alumnoId){
        Session session = factory.openSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();
            Alumnos alumno = (Alumnos)session.get(Alumnos.class, alumnoId);
            session.delete(alumno);
            tx.commit();
        } catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
    }
}
