package com.apptec.ejemplo;

import com.apptec.ejemplo.client.escolaresTest;
import com.google.gwt.junit.tools.GWTTestSuite;
import junit.framework.Test;
import junit.framework.TestSuite;

public class escolaresSuite extends GWTTestSuite {
  public static Test suite() {
    TestSuite suite = new TestSuite("Tests for escolares");
    suite.addTestSuite(escolaresTest.class);
    return suite;
  }
}
