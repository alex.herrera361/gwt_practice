// Automatically Generated -- DO NOT EDIT
// com.apptec.ejemplo.client.EscolaresRequestFactory
package com.apptec.ejemplo.client;
import java.util.Arrays;
import com.google.web.bindery.requestfactory.vm.impl.OperationData;
import com.google.web.bindery.requestfactory.vm.impl.OperationKey;
import com.google.gwt.core.shared.GwtIncompatible;
@GwtIncompatible("Server-side only but loaded through naming convention so must be in same package as shared EscolaresRequestFactory interface")
public final class EscolaresRequestFactoryDeobfuscatorBuilder extends com.google.web.bindery.requestfactory.vm.impl.Deobfuscator.Builder {
{
withOperation(new OperationKey("PIZkM6vhGnSNdE3cCEuFeFIecKs="),
  new OperationData.Builder()
  .withClientMethodDescriptor("()Lcom/google/web/bindery/requestfactory/shared/Request;")
  .withDomainMethodDescriptor("()Ljava/lang/String;")
  .withMethodName("holi")
  .withRequestContext("com.apptec.ejemplo.client.AlumnoContext")
  .build());
}}
